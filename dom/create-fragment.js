/*
 * Tagged template function allowing you to write in an html string
 * and be returned a document fragment
 */

/* Usage:

Example 1 (using tagged template function):

let toRender = {
	key: 'Test',
	value: 'some text'
};

let node = frag`
<div>
rogue text
	<h2>${toRender.key}</h2>
	<span>${toRender.value}</span>
</div>
<label>Input label</label>
<input placeholder="input placeholder" class="input-style" disabled checked/>
<div>
	<ul>
		<li><ins>list item 1</ins></li>
		<li><del>list item 2</del></li>
	</ul>
</div>
`;

document.querySelector('body').appendChild(node);


Example 2 (just using the classes directly):

let element = new FragmentElement('h2', {
	textContent: 'test h2',
	styles: {
		color: 'blue'
	}
});

let fragment = new DocumentFragment(element);
let node = fragment.node;
document.querySelector('body').appendChild(node);
*/

export class DocumentFragment {
	constructor(elements) {
		this._fragment = document.createDocumentFragment();

		for (let el of [elements].flat()) {
			this._fragment.appendChild(el.element)
		}
	}
	get node() {
		return this._fragment;
	}
}

export class FragmentElement {
	constructor(tagName, {
		attributes = {},
		children = [],
		classList = [],
		dataset = {},
		id,
		props = [],
		styles = {},
		textContent,
	} = {}) {
		this._el = document.createElement(tagName);
		
		if (id) this._el.id = id;
		
		for (let attr in attributes) {
			this._el.setAttribute(attr, attributes[attr]);
		}
		
		for (let dataAttr in dataset) {
			this._el.dataset[dataAttr] = dataset[dataAttr];
		}
		
		for (let prop of props) {
			this._el[prop] = true;
		}
		
		for (let className in classList) {
			this._el.classList.add(className);
		}
		
		for (let styleKey in styles) { // styleKey needs to be kebab case
			this._el.style.setProperty(styleKey, styles[styleKey]);
		}
		
		if (textContent) {
			this.addTextContent(textContent);
		}
		
		children.forEach(this.addChild);
	}
	get element() {
		return this._el;
	}
	addChild(child) {
		this._el.appendChild(child.element);
	}
	addTextContent(text) {
		this._el.appendChild(document.createTextNode(text));
	}
}

function last(arr) {
	return arr[arr.length - 1];
}

export default function frag(strings, ...exps) {
	// reconstruct the full string passed in from the strings and expression results
	const fullString = strings.reduce((acc, str, i) => acc + str + (exps[i] || ''), '');

	const openTags = [];
	const activeElts = [];
	const elements = [];
	let remainingString = fullString.trim();
	let parsingError;
	
	const openingRegex = /^(([\s|\w]*)<\s*(\w+)(.*?)>)/; // find and opening tag, extracting the tag name, possible attributes/props, and possible text before
	const selfClosingRegex = /^(([\s|\w]*)<\s*(\w+)(.*?)\/>)/; // find a self closing tag, extracting the tag name, possible attributes/props, and possible text before
	const closingRegex = /^(([\s|\w]*)<\/\s*(\w+).*?>)/; // find a closing tag, extracting the tag name and possinle text before
	
	// given the tag string not including the tafname, extract attrs and props
	function parseTagString(str) {
		const parsedOptions = {
			attributes: {},
			dataset: {},
		};
	
		const attrMatch = str.match(/(\s+[\w-]+(\=\".+?\"))/g);
		if (attrMatch) {
			attrMatch.forEach(matchStr => {
				const [, key, val] = matchStr.match(/([\w-]+)\=\"(.+?)\"/);
				if (key === 'id') {
					parsedOptions[key] = val;
				}
				else if (key.startsWith('data-')) {
					parsedOptions.dataset[key.slice('data-'.length)] = val;
				}
				else if (key === 'class') {
					parsedOptions.classList = val.split(' ');
				}
				else if (key === 'style') {
					const styleMatches = val.match(/([\w-]+):\s(.+?);/gm);
					styleMatches.forEach(declaration => {
						const [, styleKey, styleVal] = declaration.match(/([\w-]+):\s(.+?);/);
						parsedOptions.styles[styleKey] = styleVal;
					});
				}
				else {
					parsedOptions.attributes[key] = val;
				}
			});
			str = str.replace(/(\s+[\w-]+(\=\".+?\"))/g, '');
		}
		
		const propMatches = str.match(/[\w-]+/g);
		if (propMatches) parsedOptions.props = propMatches;
		
		return parsedOptions;
	}
	
	// create new fragment element and keep track of newly opened tag
	function addOpeningTag(tagName, remainingTagStr, textContent = '') {
		openTags.push(tagName);
		const fragmentOpts = parseTagString(remainingTagStr);
		const fragment = new FragmentElement(tagName, fragmentOpts);
		const currentActive = last(activeElts);
		activeElts.push(fragment);
		
		if (textContent.trim().length) {
			if (currentActive) {
				currentActive.addTextContent(textContent);
			}
			else {
				parsingError = 'Can\'t handle text not outside of element';
			}
		}
		
		if (currentActive) {
			currentActive.addChild(fragment);
		}
		else {
			elements.push(fragment);
		}
	}
	
	// update opened tags and possibly set text content
	function closeLastTag(textContent = '') {
		openTags.pop();
		const fragment = activeElts.pop();

		if (textContent.trim().length) {
			fragment.addTextContent(textContent);
		}
	}

	// loop through string checking for opening/closing tags
	// after finding a match, remove from the string and continue until all parsed or an error
	while (remainingString.length && !parsingError) {
		const openMatch = remainingString.match(openingRegex);
		const selfClosingMatch = remainingString.match(selfClosingRegex);
		const closingMatch = remainingString.match(closingRegex);
	
		if (selfClosingMatch) {
			remainingString = remainingString.slice(selfClosingMatch[1].length);
			addOpeningTag(selfClosingMatch[3], selfClosingMatch[4], selfClosingMatch[2]);
			closeLastTag();
		}
		else if (openMatch) {
			remainingString = remainingString.slice(openMatch[1].length);
			addOpeningTag(openMatch[3], openMatch[4], openMatch[2]);
		}
		else if (closingMatch) {
			if (closingMatch[3] !== last(openTags)) {
				parsingError = `Unclosed tag: ${last(openTags)}`;
			}
			else {
				remainingString = remainingString.slice(closingMatch[1].length);
				closeLastTag(closingMatch[2]);
			}
		}
		else {
			parsingError = `Could not find opening or closing tag in string: ${remainingString}`;
		}
	}
	
	if (parsingError) {
		throw new Error(parsingError);
	}
	
	const fragment = new DocumentFragment(elements);
	return fragment.node;
}
